from django.conf.urls import url
from .views import ProjectListAndFormView
from django.urls import path
from . import views


urlpatterns = [
    url(r'^$', ProjectListAndFormView.as_view(), name='main'),
]
